const app = require('./../app');
//const logger = require('../logger');
const config = require('./../config');

app.listen(config.PORT, () => {
  console.log('Application running on port 3000');
});
