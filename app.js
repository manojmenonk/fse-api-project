const express = require('express');
const mongoose = require('mongoose');
const taskRoutes = require('./api/routes/task.routers');
const config = require('./config');
const cors = require('cors');
const connectionString = `${config.MONGODB_URL}/${config.MONGODB_DBNAME}`;
console.log("connectionString:"+connectionString);
mongoose.Promise = global.Promise;
mongoose.connect(connectionString)
  .then(() => {
    console.log('Successfully connected to the database'+connectionString);
  }).catch((er) => {
    console.log('Could not connect to the database. Stopping application startup.');
    console.log(er);
    process.exit();
  });
  mongoose.set('debug', true);
  const app = express();
  // app.use((req, res, next) => {
  //   // fix for cross origin
  //     res.setHeader('Access-Control-Allow-Origin', '*');
  //     res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
  //     res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, x-auth');
  //     next();
  //   });
  // const originsWhitelist = [
  //   'http://localhost:8080', // this is my front-end url for development
  
  // ];
  // const corsOptions = {
  //   origin(origin, callback) {
  //     const isWhitelisted = originsWhitelist.indexOf(origin) !== -1;
  //     callback(null, isWhitelisted);
  //   },
  //   credentials: true,
  // };
  //  app.use(cors(corsOptions));
  app.use(cors());
  // log all incomming requests
  app.use((req, res, next) => {
    console.log(`${req.method} - ${req.url} being requested`);
    next();
  });
  // check if the api is up and running
  app.get('/healthcheck', (req, res) => {
    res.send('Application is Working Fine');
  });

  // handle path '/tasks'
  taskRoutes(app);
  module.exports = app;
