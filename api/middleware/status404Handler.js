const status404Handler = (err, req, res, next) => {
  if (err.statusCode === 404) {
    res.status(404).json({ message: err.message });
  } else {
    next(err);
  }
};

module.exports = status404Handler;
