
const status404Handler = require('./status404Handler');
const status500Handler = require('./status500Handler');
const status400Handler = require('./status400Handler');

// module.exports = router;
module.exports = (app) => {
  app.use(status400Handler);
  app.use(status500Handler);
  app.use(status404Handler);
  return app;
};