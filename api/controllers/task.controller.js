// const mongoose = require('mongoose');

/* parent Task API routes */

const express = require('express');
var isodate = require('isodate');
taskController = express.Router();
    url = require('url');

const Task = require('../models/task.model');
const ParentTask = require('../models/parenttask.model');

// add new task
  const createTask = async (req, res) => {
  let newTask = new Task(req.body);

  newTask.save()
      .then(newTask => {
          res.status(200).json({ 'Success': true })
      })
      .catch(err => {
          res.status(400).send({ 'Success': false, 'Message': err });
      });
};

//get All task
  const getAllTasks = async (req, res) => {
    var taskQuery = Task.find();

    var queryparams = req.query;
    //console.log("queryparams.criteria::"+queryparams.criteria);
    //console.log("queryparams.searchKey::"+queryparams.searchKey);
    
            if (queryparams.sortKey) {
                var sortdirection = 1;
                if(queryparams.sortKey=="Status"){
                    sortdirection = -1;
                }
                taskQuery.sort([[queryparams.sortKey, sortdirection]]);
            }
            //Search task
            if (queryparams.searchKey) {
                if(queryparams.criteria == "Task" ) {
                    taskQuery.or([
                        { 'Task': { $regex: queryparams.searchKey, $options: 'i' } }]);
                }  
                if(queryparams.criteria == "Priority" ) {
                    console.log("inside priroty : "+queryparams.searchKey);
                    const str = queryparams.searchKey;
                    const lftStr = str.substring(0, str.indexOf(":"));
                    const rgtStr = str.split(":").pop();
                    console.log("lftStr : "+lftStr);
                    console.log("rgtStr : "+rgtStr);
                    taskQuery.or([
                        { 'Priority': { $gte : lftStr, $lte : rgtStr } }]);
                } 

                if(queryparams.criteria == "StartDate" ) {
                    const dateStr = queryparams.searchKey;
                    taskQuery.or([
                        { 'Start_Date': { $gte : isodate(dateStr)} }]);
                } 
                if(queryparams.criteria == "EndDate" ) {
                    const dateStr = queryparams.searchKey;
                    taskQuery.or([
                        { 'End_Date': { $lte : isodate(dateStr) } }]);
                }
                  
            }
            taskQuery.populate('Parent');
            taskQuery.exec(function (err, tasks) {

                if (err) {
                    res.json({ 'Success': false })
                }
                else {
                    res.json({ 'Success': true, 'Data': tasks });
                }
            });
     
};

//get parent search
const searchParentTask = async (req,res)=>{
    var queryparams = req.query;
    if(queryparams.criteria == "ParentTask" ) {
        console.log("inside parent : "+queryparams.searchKey);
        ParentTask.aggregate([{
              $match: {
                Parent_Task: {
                  $in: ["Today task"]
                }
              }},
            {
              $lookup: {
                from: "tasks",
                localField: "_id",
                foreignField: "Parent",
                as: "parenttaskobject"
              }},
            {
              $unwind: "$parenttaskobject"
            },
            {
                $project: {
                    _id: "$parenttaskobject._id",
                    Start_Date:"$parenttaskobject.Start_Date",
                    End_Date:"$parenttaskobject.End_Date",
                    Status:"$parenttaskobject.Status",
                    Parent_ID:"$parenttaskobject.Parent_ID",
                    Task:"$parenttaskobject.Task",
                    Priority:"$parenttaskobject.Priority",
                    Parent:"$parenttaskobject.Parent",
                    Task_ID:"$parenttaskobject.Task_ID",
                    Parent: {Parent_Task:"$Parent_Task"}    
                    
                }
            }
          ], function (err, tasks) {

            if (err) {
                res.json({ 'Success': false })
            }
            else {
                res.json({ 'Success': true, 'Data': tasks});
                //console.log("result:"+parenttaskobject);
            }
        }) 
    }
}

//get single task
  const getTask = async (req, res) => {
  let taskId = req.params.id;

  var taskQuery = Task.findOne({ Task_ID: taskId })
      .populate('Parent');

  taskQuery.exec(function (err, task) {
      if (err) {
          res.json({ 'Success': false, 'Message': 'task not found' })
      }
      else {
          res.json({ 'Success': true, 'Data': task });
      }
  });
};

// udate task
  const updateTask = async (req,res) => {
  let updateTask = new Task(req.body);

  Task.findOne({ Task_ID: updateTask.Task_ID }, function (err, task) {
     
      task.Task = updateTask.Task;
      task.Priority = updateTask.Priority;
      task.Start_Date = updateTask.Start_Date;
      task.End_Date = updateTask.End_Date;
      task.Parent = updateTask.Parent;

      task.save().then(updateTask => {
          res.status(200).json({ 'Success': true })
      })
      .catch(err => {
          res.status(400).send({ 'Success': false, 'Message': 'Error occured while updating doc' });
      });

  });
};

//end task
  const deleteTask = async (req, res) => {
  let taskId = req.params.id;

  Task.findOne({ Task_ID: taskId }, function (err, task) {
     
      task.Status = 1;
      
      task.save().then(updateTask => {
          res.status(200).json({ 'Success': true })
      })
      .catch(err => {
          res.status(400).send({ 'Success': false, 'Message': 'Error occured while updating doc' });
      });

  });
};

//module.exports = taskController;

module.exports = {
  createTask,
  updateTask,
  getAllTasks,
  searchParentTask,
  getTask,
  deleteTask
};
