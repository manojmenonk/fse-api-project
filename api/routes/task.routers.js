const express = require('express');
const bodyParser = require('body-parser');

const ParentTaskController = require('../controllers/parenttask.controller');
const TaskController = require('../controllers/task.controller');

const router = express.Router();

router.use(bodyParser.json());
router.use(bodyParser.urlencoded({ extended: false }));

router.post('/parenttasks/add', ParentTaskController.createParentTask);
router.get('/parenttasks/:id', ParentTaskController.getParentTask);
router.get('/parenttasks', ParentTaskController.getAllParentTasks);

router.post('/tasks/add', TaskController.createTask);
router.get('/tasks/:id', TaskController.getTask);
router.get('/tasks', TaskController.getAllTasks);
router.get('/searchParent', TaskController.searchParentTask);
router.post('/tasks/edit', TaskController.updateTask);
router.get('/tasks/delete/:id', TaskController.deleteTask);

module.exports = (app) => {
  app.use(router);
  return app;
};
