require('should');
const request = require('supertest');
const { expect } = require('chai');
const express = require('express');
const mongoose = require('mongoose');
const taskRoutes = require('./task.routers');
const TaskModel = require('../models/task.model');
const ParentTaskModel = require('../models/parenttask.model');
const app = require('./../../app');
function json(verb, url) {
    return request(app)[verb](url)
      .set('Content-Type', 'application/json')
      .set('Accept', 'applicaiton/json')
      .expect('Content-Type', /json/);
  }

  describe('Task Routes', () => {
    
    before((done) => {
        TaskModel.deleteMany({}).exec();
        ParentTaskModel.deleteMany({}).exec();
      done();
    });

    it('should be able to add a Parent task', (done) => {
        const parentTask = { Parent_Task: 'sample first Parent task' };
        json('post', '/parenttasks/add')
          .send(parentTask)
          .expect(200)
          .end((err, res) => {
            if (err && Object.keys(err).length > 0) {
             console.log(`res.err  -->${JSON.stringify(err)}`);
            }
            if (res && res.body && Object.prototype.hasOwnProperty.call(res.body, 'error')) {
              console.log(`res.body -->${JSON.stringify(res)}`);
            }
            expect(res.body).to.not.equal(null);
    
            parentTaskID = res.body._id;
    
            done();
          });
      });
      
      it('should be able to get all the parent tasks', (done) => {
        json('get', '/parenttasks')
          .send()
          .expect(200)
          .end((err, res) => {
            if (err && Object.keys(err).length > 0) {
                console.log(`res.err  -->${JSON.stringify(err)}`);
            }
            if (res && res.body && Object.prototype.hasOwnProperty.call(res.body, 'error')) {
                console.log(`res.body -->${JSON.stringify(res)}`);
            }
            expect(res.body).to.not.equal(null);
            done();
          });
      });

      it('should be able to add a Task', (done) => {
        const task = { Task: 'sample first task' };
        json('post', '/tasks/add')
          .send(task)
          .expect(200)
          .end((err, res) => {
            if (err && Object.keys(err).length > 0) {
             console.log(`res.err  -->${JSON.stringify(err)}`);
            }
            if (res && res.body && Object.prototype.hasOwnProperty.call(res.body, 'error')) {
              console.log(`res.body -->${JSON.stringify(res)}`);
            }
            expect(res.body).to.not.equal(null);
    
            TaskID = res.body._id;
    
            done();
          });
      });

      it('should be able to get all the tasks', (done) => {
        json('get', '/tasks')
          .send()
          .expect(200)
          .end((err, res) => {
            if (err && Object.keys(err).length > 0) {
                console.log(`res.err  -->${JSON.stringify(err)}`);
            }
            if (res && res.body && Object.prototype.hasOwnProperty.call(res.body, 'error')) {
                console.log(`res.body -->${JSON.stringify(res)}`);
            }
            expect(res.body).to.not.equal(null);
            done();
          });
      });

})