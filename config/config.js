const fs = require('fs');
const path = require('path');

const env = process.env.NODE_ENV || '';
console.log("env:"+env);
let configPath = path.join(__dirname, `config.${env}.json`);
console.log("configPath:"+configPath);
if (!fs.existsSync(configPath)) {
  configPath = path.join(__dirname, 'config.json');
}
const configFile = fs.readFileSync(configPath, { encoding: 'utf8' });
const envConfig = JSON.parse(configFile);

// reset environment variables if provided
envConfig.PORT = process.env.PORT || envConfig.PORT;
envConfig.MONGODB_URL = process.env.MONGODB_URL || envConfig.MONGODB_URL;

const config = envConfig;


module.exports = config;