require('should');
const request = require('supertest');
const app = require('./app');

describe('App', () => {
  it('should return text when requested for health check path', (done) => {
    request(app)
      .get('/healthcheck')
      .set('Accept', 'application/json')
      .expect('Content-Type', /text/)
      .expect(200)
      .end((err, res) => {
        res.text.should.be.exactly('Application is Working Fine');
        done();
      });
  });
});